// Опишіть, як можна створити новий HTML тег на сторінці.
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Як можна видалити елемент зі сторінки?

// 1. Новий HTML тег на странице создается с помощью метода
// document.createElement(tag).
// 2. Первый параметр ф-ции elem.insertAdjacentHTML(where, html) означает, куда именно нужно вставить элемент:
//  "beforebegin" - прямо перед elem
//  "afterbegin"  - в начало elem
//  "beforeend" - в конец elem
//  "afterend" - сразу после elem
// 3. Элемент можно удалить с помощью метода elem.remove()



let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let list = function() {
    let ul = document.createElement("ul");
    let listElems = arr.map(element => {
    return document.getElementById("par").insertAdjacentHTML("afterend",`<li> ${element} </li>`)
}).join(""); 
return listElems;
};
let list1 = list(arr);

// ul.innerHTML = arr.map(element => {
//     return `<li> ${element} </li>`
// }).join(""); 
// console.log(ul.innerHTML);
// // 1 variant
// document.body.prepend(ul); 
//  setTimeout(() => ul.remove(), 3000);
